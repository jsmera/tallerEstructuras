#include<iostream>
#include "funciones.h"
using namespace std;


int main() {
	bool live = true;
	int choose, n, c;
	cout << "Enter:" << endl <<"1 for ejercicio2" << endl <<"2 for ejercicio 3" << endl <<"3 for puntoMasCercano" << endl <<"4 for balanceado"<<endl;

	while (live) {
		cout << "Option: ";
		cin >> choose;
		switch (choose) {
		case 1:
			// ejercicio 2 la persona digita un numero, y esta le retorna 1 o 0, 1 si es 
			//primo 0 si no es
			cout << "Enter a number: " << endl;
			cin >> n;
			cout << ejercicio2(n) << endl;
			break;
		case 2:
			cout << "Enter a number for first primes numbers: " << endl;
			cin >> c;
			ejercicio3(c);
			break;
		case 3:
			int size_vector, *point;
			cout << "Size of vectors: ";
			cin >> size_vector;
			int *coords_x, *coords_y;
			coords_x = (int*)malloc(sizeof(int)*size_vector);
			coords_y = (int*)malloc(sizeof(int)*size_vector);

			for (int i = 0; i < size_vector; i++) {
				int x, y;
				cout << "Position in x: ";
				cin >> x;
				cout << "Position in y: ";
				cin >> y;
				coords_x[i] = x;
				coords_y[i] = y;
			}

			point = puntoMasCercano(coords_x, coords_y, size_vector);

			cout << "Point more near: " << point[0] << ", " << point[1] << endl;

			break;
		case 4:
			int size_array;
			cout << "please enter size of vector" << endl;
			cin >> size_array;
			int *array1;
			array1 = (int*)malloc(sizeof(int)*size_array);
			int x;// valores que tiene el array;
			for (int i = 0; i < size_array; i++) {
				cout << "enter values: ";
				cin >> x;
				array1[i] = x;
			}
			balanceado(array1,size_array);
			break;
		default:
			live = false;
		}
	}
}

