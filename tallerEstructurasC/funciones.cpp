#include <iostream>
#include <vector>
#include <cmath>
#include "funciones.h"

using namespace std;

int ejercicio2(int n) {  
	// Empieza en verdadero 
	bool result = true;
	int divisor = 2;
	// El siguiente ciclo busca un divisor hasta la mitad de n
	while (result && divisor <= n / 2) {
		// Si el numero divisor es divisor de n entonces result sera falso
		if (n%divisor == 0)
			result = false;
		// De lo contrario aumentara el divisor para seguir en la busqueda
		else
			divisor++;
	}
	// En caso de que result sea verdadeo y el n diferente de 1
	// entonces n es primo
	if (result && n != 1) 
		return 1;
	// De lo contrario n es no primo
	else
		return 0;
}

void ejercicio3 (int c) {
	// c es el numero maximo donde se buscaran los primos
	vector <int> primos;
	for (int i = 1; i <= c; i++) {
		// Si encuentra un primo lo guarda en el vector
		if (ejercicio2(i)) {
			primos.push_back(i);
		}
	}

	for (unsigned int x = 0; x < primos.size(); x++) {
		cout << primos[x] << endl;
	}
}

int * puntoMasCercano(int vector_x[], int vector_y[], int n) {
	// Referencia https://www.tutorialspoint.com/cplusplus/cpp_return_arrays_from_functions.htm
	// Se crea un arreglo con 2 posiciones
	// Este arreglo contiene el punto mas cercano
	static int result[2];
	// Nuevo arreglo para las distancias
	int *distance = (int *) malloc(sizeof(int)*n);
	// Se calculo la distancia por cada punto
	// Cada indixe de en este arreglo corresponde a cada
	// coordenada (x, y) en su respectivos arreglos
	for (int i = 0; i < n; i++) {
		distance[i] = sqrt(vector_x[i] * vector_x[i] + vector_y[i] * vector_y[i]);
	}

	// Se calcula la minima distancia
	int minimun = distance[0];
	result[0] = vector_x[0];
	result[1] = vector_y[0];
	for (int i = 1; i < n-1; i++) {
		if (minimun >= distance[i+1]) {
			// Leer linea 52 y 54s
			result[0] = vector_x[i+1];
			result[1] = vector_y[i+1];
			minimun = distance[i];
		}
	}
	// Se retorna el punto mas cercano
	return result;
}
void balanceado(int arrayV[], int size) {
	int mitad;
	int sumLeft, sumRight; 
	sumLeft = sumRight = 0;
	//condicion 1
	bool cond1 = false;
	//condicion 2
	bool cond2 = false;
	mitad = (size / 2);
	//recorre el array hasta la primer mitad 
	for (int i = 0; i < mitad; i++) 
		//suma la primer mitad del arreglo
		sumLeft += arrayV[i];
	//recorre el array desde donde empieza la segunda mitad hasta size
	for (int x = mitad; x < size; x++) 
		//suma la segunda mitad del arreglo
		sumRight += arrayV[x];

	/* primera condicion para un array ser balanceado,necesita
	que la suma de la mitad izquierda sea menor que el de la derecha,
	cuando este se multiplica por 2*/
	if (sumLeft < 2 * sumRight)
		cond1 = true;

	int condicional = sumRight / 2;
	/*segunda condicion, la suma de la mitad izquierda debe ser por lo menos la mitad de la suma
	de los elementos de la mitad derecha*/
	if (sumLeft >= condicional)
		cond2 = true;
	/*si estas dos se cumplen esto quiere decir que es balanceado*/
	if (cond1 && cond2)
		cout << "is a balanced array" << endl;
	else 
		cout << "is not balanced" << endl;
}